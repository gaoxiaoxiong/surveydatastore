# 调研DataStore能否替换SharedPreferences


#### 目的
现在使用SharedPreferences导致的ANR问题比较多，调研使用DataStore替换SharedPreferences能否解决ANR，所以是需要先了解，SharedPreferences如何ANR与DataStore如何解决ANR问题

#### 目录
- SharedPreferences 为啥会ANR
- DataStore 为啥可以解决SharedPreferences的ANR
- 总结

#### SharedPreferences 为啥会ANR
我们先看SharedPreferences的创建
```
使用
context.getSharedPreferences(configName, Context.MODE_PRIVATE);

ContextImpl.java
@Override
    public SharedPreferences getSharedPreferences(String name, int mode) {
        if (mPackageInfo.getApplicationInfo().targetSdkVersion <
                Build.VERSION_CODES.KITKAT) {
            if (name == null) {
                name = "null";
            }
        }

        File file;
        synchronized (ContextImpl.class) {
            if (mSharedPrefsPaths == null) {
                mSharedPrefsPaths = new ArrayMap<>();
            }
            file = mSharedPrefsPaths.get(name);
            if (file == null) {
                file = getSharedPreferencesPath(name);//创建文件
                mSharedPrefsPaths.put(name, file);
            }
        }
        //拿到 SharedPreferences
        return getSharedPreferences(file, mode);
    }

    @Override
    //创建文件
    public File getSharedPreferencesPath(String name) {
        return makeFilename(getPreferencesDir(), name + ".xml");
    }

```
从这里可以看出，有使用**synchronized**关键字进行加锁，如果我们初始化的时机是在，Application的onCreate，此时我们的file == null
```
 @Override
    public SharedPreferences getSharedPreferences(File file, int mode) {
        SharedPreferencesImpl sp;
        synchronized (ContextImpl.class) {
            final ArrayMap<File, SharedPreferencesImpl> cache = getSharedPreferencesCacheLocked();
            sp = cache.get(file);
            if (sp == null) {
                checkMode(mode);
                if (getApplicationInfo().targetSdkVersion >= android.os.Build.VERSION_CODES.O) {
                    if (isCredentialProtectedStorage()
                            && !getSystemService(UserManager.class)
                                    .isUserUnlockingOrUnlocked(UserHandle.myUserId())) {
                        throw new IllegalStateException("SharedPreferences in credential encrypted "
                                + "storage are not available until after user is unlocked");
                    }
                }
                sp = new SharedPreferencesImpl(file, mode);
                cache.put(file, sp);
                return sp;
            }
        }
        if ((mode & Context.MODE_MULTI_PROCESS) != 0 ||
            getApplicationInfo().targetSdkVersion < android.os.Build.VERSION_CODES.HONEYCOMB) {
            sp.startReloadIfChangedUnexpectedly();
        }
        return sp;
    }
``` 
由于是第一次进行初始化，所以sp == null ，会去创建SharedPreferencesImpl()，在构造函数里面走startLoadFromDisk()的逻辑
```
SharedPreferencesImpl.java

    SharedPreferencesImpl(File file, int mode) {
        mFile = file;
        mBackupFile = makeBackupFile(file);
        mMode = mode;
        mLoaded = false;
        mMap = null;
        mThrowable = null;
        startLoadFromDisk();
    }

    private void startLoadFromDisk() {
        synchronized (mLock) {
            mLoaded = false;
        }
        //开线程加载本地文件
        new Thread("SharedPreferencesImpl-load") {
            public void run() {
                loadFromDisk();
            }
        }.start();
    }

    //开始加载本地文件
    private void loadFromDisk() {
        synchronized (mLock) {
            if (mLoaded) {
                return;
            }
            if (mBackupFile.exists()) {
                mFile.delete();
                mBackupFile.renameTo(mFile);
            }
        }

        // Debugging
        if (mFile.exists() && !mFile.canRead()) {
            Log.w(TAG, "Attempt to read preferences file " + mFile + " without permission");
        }

        Map<String, Object> map = null;
        StructStat stat = null;
        Throwable thrown = null;
        try {
            stat = Os.stat(mFile.getPath());
            if (mFile.canRead()) {
                BufferedInputStream str = null;
                try {
                    str = new BufferedInputStream(
                            new FileInputStream(mFile), 16 * 1024);
                    map = (Map<String, Object>) XmlUtils.readMapXml(str);//这里很重要
                } catch (Exception e) {
                    Log.w(TAG, "Cannot read " + mFile.getAbsolutePath(), e);
                } finally {
                    IoUtils.closeQuietly(str);
                }
            }
        } catch (ErrnoException e) {
            
        } catch (Throwable t) {
            thrown = t;
        }

        synchronized (mLock) {
            mLoaded = true; //这里也很重要
            mThrowable = thrown;

            try {
                if (thrown == null) {
                    if (map != null) {
                        mMap = map;//这里也很重要
                        mStatTimestamp = stat.st_mtim;
                        mStatSize = stat.st_size;
                    } else {
                        mMap = new HashMap<>();
                    }
                }
            } catch (Throwable t) {
                mThrowable = t;
            } finally {
                mLock.notifyAll();
            }
        }
    }
```
这里比较重要的是 map = (Map<String, Object>) XmlUtils.readMapXml(str); 加载成功后，会将 mLoaded = true，map值会赋值给mMap的全局变量，到此我们已经知道了初始化的全过程，以及注意事项。
<br/>
我们还没有正式的说到，为啥SharedPreferences会导致ANR
<br/>
我们看下，平时我们如何读数据的
```
 SharedPreferences sharedPreferences= getSharedPreferences("data", Context .MODE_PRIVATE);
 String userId=sharedPreferences.getString("name","");

SharedPreferencesImpl.java
    public String getString(String key, @Nullable String defValue) {
        synchronized (mLock) {
            awaitLoadedLocked();//这里比较重要
            String v = (String)mMap.get(key);
            return v != null ? v : defValue;
        }
    }

   private void awaitLoadedLocked() {
        if (!mLoaded) {
            BlockGuard.getThreadPolicy().onReadFromDisk();
        }
        while (!mLoaded) {//这里比较重要
            try {
                mLock.wait();
            } catch (InterruptedException unused) {
            }
        }
        if (mThrowable != null) {
            throw new IllegalStateException(mThrowable);
        }
    }
```
读与写，都是使用了类锁**synchronized (mLock) 对象锁**，如果我们初始化未完成，那么mLoaded = fale 就会导致mLock.wait()触发，锁等待，我们在Activity 5秒之内没有处理完成，那么就发生了ANR的情况。
<br/>
写的情况
```
    @Override
    public Editor edit() {
        synchronized (mLock) {
            awaitLoadedLocked();
        }
        return new EditorImpl();
    }

        public Editor putString(String key, @Nullable String value) {
            synchronized (mEditorLock) {
                mModified.put(key, value);
                return this;
            }
        }

        //最终去提交
        public boolean commit() {
            long startTime = 0;

            if (DEBUG) {
                startTime = System.currentTimeMillis();
            }

            //这里比较重要
            MemoryCommitResult mcr = commitToMemory();

            //开始写
            SharedPreferencesImpl.this.enqueueDiskWrite(
                mcr, null /* sync write on this thread okay */);
            try {
                mcr.writtenToDiskLatch.await();
            } catch (InterruptedException e) {
                return false;
            } finally {
                if (DEBUG) {
                    Log.d(TAG, mFile.getName() + ":" + mcr.memoryStateGeneration
                            + " committed after " + (System.currentTimeMillis() - startTime)
                            + " ms");
                }
            }
            notifyListeners(mcr);
            return mcr.writeToDiskResult;
        }

        //提交到本地内存里面
        private MemoryCommitResult commitToMemory() {
            Map<String, Object> mapToWriteToDisk;//需要写入到.xml文件里面的字段信息
            synchronized (SharedPreferencesImpl.this.mLock) {
                //省略很多代码
                //这里的mMap是我们构造SharedPreferences，读取.xml文件出来的所有的字段信息
             if (mDiskWritesInFlight > 0) { 
                    mMap = new HashMap<String, Object>(mMap);
                }
                mapToWriteToDisk = mMap;
                mDiskWritesInFlight++;
                synchronized (mEditorLock) {
                    //mModified 很重要，是我们新put的字段信息
                    for (Map.Entry<String, Object> e : mModified.entrySet()) {
                        String k = e.getKey();
                        Object v = e.getValue();
                        if (v == this || v == null) {
                            if (!mapToWriteToDisk.containsKey(k)) {
                                continue;
                            }
                            mapToWriteToDisk.remove(k);
                        } else {
                            if (mapToWriteToDisk.containsKey(k)) {
                                Object existingValue = mapToWriteToDisk.get(k);
                                if (existingValue != null && existingValue.equals(v)) {
                                    continue;
                                }
                            }
                            //
                            mapToWriteToDisk.put(k, v);
                        }
                    }
                }
            }
            return new MemoryCommitResult(memoryStateGeneration, keysCleared, keysModified,
                    listeners, mapToWriteToDisk);
        }

        private MemoryCommitResult(long memoryStateGeneration, boolean keysCleared,
                @Nullable List<String> keysModified,
                @Nullable Set<OnSharedPreferenceChangeListener> listeners,
                Map<String, Object> mapToWriteToDisk) {
            this.memoryStateGeneration = memoryStateGeneration;
            this.keysCleared = keysCleared;
            this.keysModified = keysModified;
            this.listeners = listeners;
            this.mapToWriteToDisk = mapToWriteToDisk;
        }
```
mModified是你put的字段信息，mMap是读取.xml文件出来的所有的字段信息
<br/>
从以上可以知道，我们每次put的新字段信息，会将以前已经在.xml里面的字段，一并存储到mapToWriteToDisk
<br/>
```
private void enqueueDiskWrite(final MemoryCommitResult mcr,
                                  final Runnable postWriteRunnable) {
        final boolean isFromSyncCommit = (postWriteRunnable == null);

        final Runnable writeToDiskRunnable = new Runnable() {
                @Override
                public void run() {
                    synchronized (mWritingToDiskLock) {
                        writeToFile(mcr, isFromSyncCommit); //写文件
                    }
                    synchronized (mLock) {
                        mDiskWritesInFlight--;
                    }
                    if (postWriteRunnable != null) {
                        postWriteRunnable.run();
                    }
                }
            };

        //我们使用commit会走这里
        if (isFromSyncCommit) {
            boolean wasEmpty = false;
            synchronized (mLock) {
                wasEmpty = mDiskWritesInFlight == 1;
            }
            //说明只有一个
            if (wasEmpty) {
                writeToDiskRunnable.run();
                return;
            }
        }
        //使用apply才会走这里
        QueuedWork.queue(writeToDiskRunnable, !isFromSyncCommit);
    }

    private static final long DELAY = 100;
    public static void queue(Runnable work, boolean shouldDelay) {
        Handler handler = getHandler();
        synchronized (sLock) {
            sWork.add(work);
            if (shouldDelay && sCanDelay) {//100 毫秒后执行
                handler.sendEmptyMessageDelayed(QueuedWorkHandler.MSG_RUN, DELAY);
            } else {
                handler.sendEmptyMessage(QueuedWorkHandler.MSG_RUN);
            }
        }
    }

    //子线程
    private static Handler getHandler() {
        synchronized (sLock) {
            if (sHandler == null) {
                HandlerThread handlerThread = new HandlerThread("queued-work-looper",
                        Process.THREAD_PRIORITY_FOREGROUND);
                handlerThread.start();

                sHandler = new QueuedWorkHandler(handlerThread.getLooper());
            }
            return sHandler;
        }
    }
    
    private static class QueuedWorkHandler extends Handler {
        static final int MSG_RUN = 1;

        QueuedWorkHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            if (msg.what == MSG_RUN) {
                processPendingWork();
            }
        }
    }

    
    private static void processPendingWork() {
        long startTime = 0;

        synchronized (sProcessingWork) {
            LinkedList<Runnable> work;
            synchronized (sLock) {
                work = sWork;
                sWork = new LinkedList<>(); 
                getHandler().removeMessages(QueuedWorkHandler.MSG_RUN);
            }

            if (work.size() > 0) {
                for (Runnable w : work) {
                    w.run();
                }
            }
        }
    }
```
那么是不是，我们使用applycommit就不会ANR呢？答案，不会，依旧会ANR的可能
<br/>
```
public static void waitToFinish() {
        //省略很多代码...
        try {
            processPendingWork();
        } finally {
            StrictMode.setThreadPolicy(oldPolicy);
        }
    }
```
```
ActivityThread.java
//停止service
private void handleStopService(IBinder token) {
        mServicesData.remove(token);
        Service s = mServices.remove(token);
        if (s != null) {
            try {
                QueuedWork.waitToFinish();
            } catch (Exception e) {
               
            }
        } 
    }

//停止activity
 public void handleStopActivity(ActivityClientRecord r, int configChanges,
            PendingTransactionActions pendingActions, boolean finalStateRequest, String reason) {
        //省略很多代码...
        if (!r.isPreHoneycomb()) {
            QueuedWork.waitToFinish();
        }
        
    }
```
我们考虑一种情况，我们先在主线程apply提交文件，任务提交后会在子线程执行文件的写入，会拿到锁，当我们去finishActivity的时候，是主线程，此时的锁主线程，无法获取到，所以只能等待。 


#### DataStore 为啥可以解决SharedPreferences的ANR
简单来说明就是开线程解决，从他的构造函数可以看出来
```
使用
private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(
        name = "userSharePreFile"
)

dataStore.edit{
        it[stringPreferencesKey(key)] = "测试"
    }

public fun preferencesDataStore(
    name: String,
    corruptionHandler: ReplaceFileCorruptionHandler<Preferences>? = null,
    produceMigrations: (Context) -> List<DataMigration<Preferences>> = { listOf() }, //需要迁移的SharedPreferences文件名称
    //从这里可以看出，开了IO
    scope: CoroutineScope = CoroutineScope(Dispatchers.IO + SupervisorJob())
): ReadOnlyProperty<Context, DataStore<Preferences>> {
    return PreferenceDataStoreSingletonDelegate(name, corruptionHandler, produceMigrations, scope)
}
```

```
public suspend fun DataStore<Preferences>.edit(
    transform: suspend (MutablePreferences) -> Unit
): Preferences {
    return this.updateData {
        it.toMutablePreferences().apply { transform(this) }
    }
}

//SingleProcessDataStore.kt
 override suspend fun updateData(transform: suspend (t: T) -> T): T {
        val ack = CompletableDeferred<T>()
        val currentDownStreamFlowState = downstreamFlow.value
        val updateMsg =Message.Update(transform, ack, currentDownStreamFlowState, coroutineContext)
        actor.offer(updateMsg)
        return ack.await()
    }

fun offer(msg: T) {
        check(
            messageQueue.trySend(msg)
                .onClosed { throw it ?: ClosedSendChannelException("Channel was closed normally") }
                .isSuccess
        )
        //省略很多代码
        if (remainingMessages.getAndIncrement() == 0) {
            scope.launch {//CoroutineScope(Dispatchers.IO + SupervisorJob())
                do {
                    //省略很多代码
                    consumeMessage(messageQueue.receive())
                } while (remainingMessages.decrementAndGet() != 0)
            }
        }
    }

private val actor = SimpleActor<Message<T>>(
        scope = scope,//CoroutineScope(Dispatchers.IO + SupervisorJob())
        //省略很多代码
        ) { msg ->
        when (msg) {
            is Message.Read -> {
                handleRead(msg)
            }
            is Message.Update -> {
                handleUpdate(msg)//执行这里
            }
        }
    }

    private suspend fun handleUpdate(update: Message.Update<T>) {
        update.ack.completeWith(
            runCatching {
                when (val currentState = downstreamFlow.value) {
                    is Data -> {
                        transformAndWrite(update.transform, update.callerContext)
                    }
                    is ReadException, is UnInitialized -> {
                        if (currentState === update.lastState) {
                            readAndInitOrPropagateAndThrowFailure()
                            //写文件
                            transformAndWrite(update.transform, update.callerContext)
                        } 
                    }
                }
            }
        )
    }

    private suspend fun readAndInitOrPropagateAndThrowFailure() {
        try {
            readAndInit()
        } catch (throwable: Throwable) {
            downstreamFlow.value = ReadException(throwable)
            throw throwable
        }
    }

    private suspend fun readAndInit() {
        //...
        val updateLock = Mutex()
        var initData = readDataOrHandleCorruption()//读取值
        var initializationComplete: Boolean = false
        val api = object : InitializerApi<T> {
            override suspend fun updateData(transform: suspend (t: T) -> T): T {
                return updateLock.withLock() {
                    if (initializationComplete) {
                        throw IllegalStateException(
                            "InitializerApi.updateData should not be " +
                                "called after initialization is complete."
                        )
                    }
                    val newData = transform(initData)
                    if (newData != initData) {
                        writeData(newData)
                        initData = newData
                    }
                    initData
                }
            }
        }
        initTasks?.forEach { it(api) }
        initTasks = null 
        updateLock.withLock {
            initializationComplete = true
        }
        downstreamFlow.value = Data(initData, initData.hashCode())//这里存储已经拿到的值
    }


    private suspend fun readDataOrHandleCorruption(): T {
        try {
            return readData()
        } catch (ex: CorruptionException) {
            val newData: T = corruptionHandler.handleCorruption(ex)
            try {
                writeData(newData)
            } catch (writeEx: IOException) {
                ex.addSuppressed(writeEx)
                throw ex
            }
            return newData
        }
    }

     private suspend fun readData(): T {
        try {
            FileInputStream(file).use { stream ->
                return serializer.readFrom(stream)
            }
        } catch (ex: FileNotFoundException) {
            if (file.exists()) {
                throw ex
            }
            return serializer.defaultValue
        }
    }

```
写数据
```
private suspend fun transformAndWrite(
        transform: suspend (t: T) -> T,
        callerContext: CoroutineContext
    ): T {
        val curDataAndHash = downstreamFlow.value as Data<T>
        curDataAndHash.checkHashCode()
        val curData = curDataAndHash.value
        val newData = withContext(callerContext) { transform(curData) }
        curDataAndHash.checkHashCode()
        return if (curData == newData) {
            curData
        } else {
            writeData(newData)
            downstreamFlow.value = Data(newData, newData.hashCode())
            newData
        }
    }

```


存储参数的方式
```
public suspend fun DataStore<Preferences>.edit(
    transform: suspend (MutablePreferences) -> Unit
): Preferences {
    return this.updateData {
        it.toMutablePreferences().apply { transform(this) }
    }
}

调用者使用
private val mDefaultIo =  CoroutineScope(Dispatchers.Default + SupervisorJob())

mDefaultIo.launch {
     it[stringPreferencesKey(key)] = "123"
    }    
```
但是这样使用是可以解决ANR问题，但是这是需要开启协程才能使用，也算是一个弊端吧
<br/>
可以考虑简单的封装一下，即给java使用，又可以给kt使用
```

//同步提交
fun <T> putAny(
        dataStore: DataStore<Preferences>,
        map: MutableMap<String, T>
    ) {
        runBlocking {
            dataStore.edit{
                for (mutableEntry in map.iterator()) {
                    val key = mutableEntry.key
                    it[stringPreferencesKey(key)] = mGson.toJson(mutableEntry.value)
                }
            }
        }
    }

private val mDefaultIo =  CoroutineScope(Dispatchers.Default + SupervisorJob())

//异步提交
fun <T> applyPutAny(
        dataStore: DataStore<Preferences>,
        map: MutableMap<String, T>
    ) {
        mDefaultIo.launch {
            dataStore.edit{
                for (mutableEntry in map.iterator()) {
                    val key = mutableEntry.key
                    it[stringPreferencesKey(key)] = mGson.toJson(mutableEntry.value)
                }
            }
        }
    }
```
对于同步的提交，这里使用了runBlocking，但是使用runBlocking会有一个缺点，就是也会卡主当前线程。。。如果我们在协程里面的话，最好就是开启一个withContext(IO){}，在java里面直接调用putAny方法就可以，缺点就是会卡主线程，但是如果你使用异步的提交方式，可以完全避免这个问题。目前DataStore还有一个好处就是，未来会支持多进程通信

#### 总结
1、DataStore替换SharedPreferences成本
<br/>
如果只改公司底层库的情况下，直接替换DataStore意义不是特别的大，依旧还是会存在ANR的可能性（采用同步提交的方式），我们还可以考虑其他方案，
- 1.1 减少SharedPreferences访问次数，可以在存储的时候，多做一个判断比较，如果相同就不插入数据
- 1.2 减少SharedPreferences存入大量信息的情况
- 1.3 减少单个文件大小（这个目前公司是有做的）
<br/>
2、替换完DataStore后，需要注意什么
<br/>
当我们将SharedPreferences替换成DataStore，就不要再使用SharedPreferences了，否则会导致写入的数据，依旧在SharedPreferences里面
<br/>








